Source: cjs
Section: interpreters
Priority: optional
Maintainer: Debian Cinnamon Team <debian-cinnamon@lists.debian.org>
Uploaders:
 Maximiliano Curia <maxy@debian.org>,
 Margarita Manterola <marga@debian.org>,
 Fabio Fantoni <fantonifabio@tiscali.it>,
Build-Depends:
 autoconf-archive,
 automake,
 debhelper (>= 11~),
 gnome-pkg-tools,
 gobject-introspection (>= 1.46.0),
 libcairo2-dev,
 libgirepository1.0-dev (>= 1.46.0),
 libglib2.0-dev (>= 2.42.0),
 libgtk-3-dev,
 libmozjs-52-dev,
 libreadline-dev,
 pkg-kde-tools,
 pkg-config,
Standards-Version: 4.1.4
Homepage: http://cinnamon.linuxmint.com/
Vcs-Browser: https://salsa.debian.org/cinnamon-team/cjs
Vcs-Git: https://salsa.debian.org/cinnamon-team/cjs.git

Package: cjs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Mozilla-based javascript bindings for the GNOME platform
 Makes it possible for applications to use all of GNOME's platform
 libraries using the Javascript language. It's mainly based on the
 Mozilla javascript engine and the GObject introspection framework.
 .
 This package is the frozen version of gjs that the cinnamon desktop currently
 uses.
 .
 This package contains the interactive console application.
Breaks: cinnamon (<< 3.8)

Package: libcjs-dev
Section: libdevel
Architecture: any
Depends:
 cjs,
 libcairo2-dev,
 libcjs0 (= ${binary:Version}),
 libgirepository1.0-dev (>= 1.41.4),
 libglib2.0-dev (>= 2.42.0),
 libgtk-3-dev,
 libmozjs-52-dev,
 ${misc:Depends},
Description: Mozilla-based javascript bindings for the Cinnamon platform
 Makes it possible for applications to use all of Cinnamon's platform
 libraries using the Javascript language. It's mainly based on the
 Mozilla javascript engine and the GObject introspection framework.
 .
 This package is the frozen version of gjs that the cinnamon desktop currently
 uses.
 .
 This package contains the development files applications need to
 build against.

Package: libcjs0
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: gir1.2-gtk-3.0, ${misc:Depends}, ${shlibs:Depends}
Provides: ${cjs:Provides}
Breaks: cinnamon (<< 3.8)
Description: Mozilla-based javascript bindings for the GNOME platform
 Makes it possible for applications to use all of GNOME's platform
 libraries using the Javascript language. It's mainly based on the
 Mozilla javascript engine and the GObject introspection framework.
 .
 This package is the frozen version of gjs that the cinnamon desktop currently
 uses.
 .
 This is the shared library applications link to.
