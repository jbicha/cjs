Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cjs
Upstream-Contact: Clement Lefebvre <clement.lefebvre@linuxmint.com>
Source: https://github.com/linuxmint/cjs

Files: *
Copyright: 2017, Chun-wei Fan
           2014, Colin Walters <walters@verbum.org>
           2013-2017, Endless Mobile, Inc
           2011-2013, Giovanni Campagna
           2012-2013, Giovanni Campagna <scampa.giovanni@gmail.com>
           2013, Intel Corporation
           2011, Jasper St. Pierre
           2016-2017, Philip Chimento
           2017, Philip Chimento <philip.chimento@gmail.com>
           2017, Philip Chimento <philip.chimento@gmail.com> <philip@endlessm.com>
           2008-2016, Pivotal Labs
           2009-2014, Red Hat, Inc
           2006-2007, Zeh Fernando and Nate Chatellier
           2008-2010, litl, LLC
           2011, Giovanni Campagna
           2011, Jasper St. Pierre
           2008, litl, LLC
License: Expat

Files: cjs/stack.cpp
       modules/console.cpp
       modules/jsUnit.js
Copyright: 2001-2004, Edward Hieatt <edward@jsunit.net>
           2008, litl, LLC
License: MPL-1.1_GPL-2+_LGPL-2.1+

Files: modules/tweener/equations.js
Copyright: 2001, Robert Penner
           2008, litl, LLC
License: BSD-3-clause

Files: git.mk
Copyright: 2010-2013, Behdad Esfahbod
           2009, Red Hat, Inc
License: Permissive

Files: debian/*
Copyright: 2009, Collabora Ltd.
License: Expat

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

License: MPL-1.1_GPL-2+_LGPL-2.1+
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Mozilla Communicator client code, released
 * March 31, 1998.
 *
 * The Initial Developer of the Original Code is
 * Netscape Communications Corporation.
 * Portions created by the Initial Developer are Copyright (C) 1999
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   John Bandhauer <jband@netscape.com> (original author)
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either of the GNU General Public License Version 2 or later (the "GPL"),
 * or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2', likewise, the
 complete text of the GNU Lesser General Public License version 2.1 can be
 found in `/usr/share/common-licenses/LGPL-2.1'.

License: Permissive
 Copying and distribution of this file, with or without modification,
 is permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.
